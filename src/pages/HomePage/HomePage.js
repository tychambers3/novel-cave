import React, { Component } from 'react'
import './HomePage.scss';
import CreateActionList from '../../components/CreateActionList/CreateActionList';
import Footer from '../../layouts/Footer/Footer';

export default class HomePage extends Component {
  render() {
    return (
      <section className="wrapper">
        <h1 className="title">What would you like to create?</h1>

        <CreateActionList/>
        <Footer/>
      </section>
    )
  }
}

