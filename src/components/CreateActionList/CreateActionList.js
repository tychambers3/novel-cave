import React from 'react';
import '../../assets/stylesheets/buttons.scss';
import './CreateActionList.scss';

const CreateActionList = () => {
  return(
    <div className="action-list">
      <button className="button fixed">Story</button>
      <button className="button fixed">Person</button>
      <button className="button fixed">Clan/Group</button>
      <button className="button fixed">Place</button>
      <button className="button fixed">Event</button>
      <button className="button fixed">Timeline</button>
      <button className="button fixed">Conflict</button>
    </div>
  );
}

export default CreateActionList;