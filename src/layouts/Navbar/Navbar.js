import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './Navbar.scss';
export default class Navbar extends Component {
  render() {
    return (
      <nav className="navbar">
        <Link to="#" className="nav-link">My People</Link>
        <Link to="#" className="nav-link">My Clans/Groups</Link>
        <Link to="#" className="nav-link">My Places</Link>
        <Link to="#" className="nav-link">My Events</Link>
        <Link to="#" className="nav-link">My Timeline(s)</Link>
        <Link to="#" className="nav-link">My Conflicts</Link>

      </nav>
    )
  }
}
