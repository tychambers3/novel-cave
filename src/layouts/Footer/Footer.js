import React, { Component } from 'react'
import Navbar from '../Navbar/Navbar';
import './Footer.scss';
export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <Navbar/>
      </footer>
    )
  }
}
